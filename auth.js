const e = require("express")


const jwt = require("jsonwebtoken")

const secret = "IDoNotKnow"

module.exports.createAccessToken = (user)=>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin:user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

// token verification - 
// next parameter - tells the sever to proceed if the verification is okay/successful
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization // authorization can be found in the headers of the request and tells whether the client/user has the quthority to send the request

    if(typeof token !== "undefined"){ // token is present
        console.log(token)
        token = token.slice(7, token.length)

        // jwt.verify - verifies the token using the secret, and fails if the token's secret does not match the secret variable -- meaning there is an attempt to hack, or at least to tamper/change the data from the user-end
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send({auth:"failed"})
            }else{
                // tells the server to proceed to processing of the request
                next()
            }
        })
    }
}

// token decoding
module.exports.decode = (token) => {
    if(typeof token !== "undefined"){ //token is present
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return null
            }else{
                return jwt.decode(token, {complete:true}).payload
                // jwt.decode - decides the data to be decoded, which is the token
                // payload - is the one that we need to verify the user information this is a part of the token when code the createAccessToken function (the one with _id, email, and isAdmin)
            }
        })
    }else{
        return null //there is no token
    }
}